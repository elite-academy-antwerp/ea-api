import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { TypeOrmModule } from "@nestjs/typeorm";
import { join } from "path";
import { ConnectionOptions } from "typeorm";
import { CommonModule } from "./common/common.module";
import { ConfigService } from "./common/config.service";
import { UserModule } from "./user/user.module";
import { AuthModule } from "./auth/auth.module";
import { PracticeModule } from './practice/practice.module';
import { TeamModule } from '@team/team.module';
import { GameModule } from './game/game.module';

@Module({
	imports: [
		TypeOrmModule.forRootAsync({
			inject: [ConfigService],
			useFactory: async (configService: ConfigService): Promise<ConnectionOptions> =>
				({
					url: configService.mongodbUri,
					entities: [join(__dirname, "**/**.entity{.ts,.js}")],
					synchronize: true,
					useNewUrlParser: true,
					logging: true,
					type: "mongodb",
					useUnifiedTopology: true
				})
		}),
		CommonModule,
		GraphQLModule.forRoot({
			typePaths: ["./**/*.graphql"],
			definitions: {
				path: join(process.cwd(), "src/graphql.ts"),
				outputAs: "class",
			},
			playground: true,
			context: ({ req }) => ({ req })
		}), 
		TeamModule,
		UserModule, 
		AuthModule,
		PracticeModule,
		GameModule
	],
	controllers: [],
	providers: []
})
export class AppModule { }
