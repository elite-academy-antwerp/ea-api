import { Logger } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import { ConfigService } from "./common/config.service";
import helmet from "helmet";

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.use(helmet())
	app.enableCors();
	const config = app.get(ConfigService);

	const options = new DocumentBuilder()
		.setTitle("Sample API")
		.setDescription("The Sample API")
		.setVersion("1.0")
		.setExternalDoc("Github repo", "https://github.com/wearetheledger")
		.build();

	const document = SwaggerModule.createDocument(app, options);
	SwaggerModule.setup("api", app, document);

	await app.listen(config.port);
	Logger.log(`🚀 Server running on http://localhost:${config.port}`, "Bootstrap");

}

bootstrap();
