import { Entity, Column, ObjectIdColumn } from 'typeorm';

@Entity()
export class Game {
    @ObjectIdColumn()
    _id: string;
    @Column()
    opponent: string;
    @Column()
    location: string;
    @Column()
    time: string;
    @Column()
    date: string;
    @Column()
    home: boolean;
    @Column()
    teamId: string;
}
