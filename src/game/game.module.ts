import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from '@team/team.entity';
import { TeamService } from '@team/team.service';
import { Game } from './game.entity';
import { GameResolver } from './game.resolver';
import { GameService } from './game.service';

@Module({
  imports: [TypeOrmModule.forFeature([Game, Team])],
  providers: [GameResolver, GameService, TeamService]
})
export class GameModule { }
