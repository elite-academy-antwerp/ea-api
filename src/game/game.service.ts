import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Game } from './game.entity';
import * as uuid from 'uuid'
import { GameInput } from './game.input';

@Injectable()
export class GameService {
    constructor(
        @InjectRepository(Game)
        private readonly gameRepository: MongoRepository<Game>,
    ) { }

    public async findAll(): Promise<Game[]> {
        return this.gameRepository.find();
    }

    public async findGameById(id: string): Promise<Game> {
        return this.gameRepository.findOne(id);
    }

    public async upcomingGameForTeam(teamId: string): Promise<Game[]> {
        return this.gameRepository.find({ teamId: teamId });
    }

    public async create(input: GameInput): Promise<Game> {
        const game = new Game();
        game._id = uuid.v4();
        game.opponent = input.opponent;
        game.time = input.time;
        game.home = input.home;
        game.location = input.location;
        game.teamId = input.teamId;
        game.date = input.date;

        return this.gameRepository.save(game);
    }
}
