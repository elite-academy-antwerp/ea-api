import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveProperty, Resolver } from '@nestjs/graphql';
import { Game as GameEntity } from './game.entity';
import { GqlAuthGuard } from '@auth/guard/auth.guard';
import { GameService } from './game.service';
import { TeamService } from '@team/team.service';
import { Game, Team } from '@graphql';
import { GameInput } from './game.input';


@Resolver('Game')
export class GameResolver {
    constructor(private readonly gameService: GameService, private readonly teamService: TeamService) { }

    @Query(() => [Game])
    @UseGuards(GqlAuthGuard)
    public async games(): Promise<Game[]> {
        return this.gameService.findAll() as any;
    }

    @Query(() => Game)
    @UseGuards(GqlAuthGuard)
    public async game(@Args('id') id: string): Promise<Game> {
        return this.gameService.findGameById(id) as any;
    }


    @Query(() => [Game])
    @UseGuards(GqlAuthGuard)
    public async upcomingGameForTeam(@Args('teamId') teamId: string): Promise<Game[]> {
        return this.gameService.upcomingGameForTeam(teamId) as any;
    }

    @Mutation(() => Game)
    @UseGuards(GqlAuthGuard)
    public async createGame(@Args("input") input: GameInput) {
        return this.gameService.create(input);
    }

    @ResolveProperty()
    public async id(@Parent() game: GameEntity) {
        const { _id } = game;
        return _id;
    }

    @ResolveProperty('team', () => Team)
    public async team(@Parent() game: GameEntity) {
        const { teamId } = game;

        return await this.teamService.findTeamById(teamId);
    }
}
