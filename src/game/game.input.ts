export class GameInput {
    opponent: string;
    location: string;
    time: string;
    date: string;
    home: boolean;
    teamId: string;
}