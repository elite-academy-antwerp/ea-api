
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export enum Role {
    ADMIN = "ADMIN",
    PLAYER = "PLAYER",
    COACH = "COACH"
}

export enum TrainingType {
    SKILL = "SKILL",
    STRENGTH = "STRENGTH",
    TEAM = "TEAM"
}

export class GameInput {
    opponent: string;
    location: string;
    time: string;
    date: string;
    home: boolean;
    teamId: string;
}

export class PracticeInput {
    date: string;
    start: string;
    end: string;
    type: TrainingType;
    location: string;
    teamId: string;
}

export class TeamInput {
    name: string;
    coachId: string;
    assistantId: string;
}

export class UserInput {
    email: string;
    password: string;
    teamId: string;
    role: Role;
}

export class Game {
    id: string;
    opponent: string;
    location: string;
    time: string;
    date: string;
    home: boolean;
    team: Team;
}

export abstract class IMutation {
    abstract login(email: string, password: string): TokenResponse | Promise<TokenResponse>;

    abstract createGame(input: GameInput): Game | Promise<Game>;

    abstract createPractice(input: PracticeInput): Practice | Promise<Practice>;

    abstract createTeam(input: TeamInput): Team | Promise<Team>;

    abstract createUser(input: UserInput): User | Promise<User>;
}

export class Practice {
    id: string;
    date: string;
    start: string;
    end: string;
    type: TrainingType;
    location: string;
    team: Team;
}

export abstract class IQuery {
    abstract games(): Game[] | Promise<Game[]>;

    abstract upcomingGameForTeam(teamId: string): Game[] | Promise<Game[]>;

    abstract game(id: string): Game | Promise<Game>;

    abstract practices(): Practice[] | Promise<Practice[]>;

    abstract practicesByTeam(teamId: string): Practice[] | Promise<Practice[]>;

    abstract practice(id: string): Practice | Promise<Practice>;

    abstract teams(): Team[] | Promise<Team[]>;

    abstract team(id: string): Team | Promise<Team>;

    abstract hello(): string | Promise<string>;

    abstract users(): User[] | Promise<User[]>;

    abstract me(): User | Promise<User>;
}

export class Team {
    id: string;
    name: string;
    coach: User;
    assistant: User;
}

export class TokenResponse {
    id_token: string;
}

export class User {
    id: string;
    email: string;
    team: Team;
    role: Role;
}
