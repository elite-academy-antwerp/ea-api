import { HttpException, HttpStatus } from "@nestjs/common";
import { ServiceException } from './service.exception';
import { ServiceErrors } from './errors';

export class UnauthorizedException extends ServiceException {
    constructor(context?: object) {
        super(ServiceErrors.UNAUTHORIZED,context)
    }   

}
