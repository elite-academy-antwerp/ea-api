import { HttpStatus } from "@nestjs/common";
import { ErrorDetails } from "./service.exception";

export enum ErrorCodes {
    UNKNOWN_ERROR = "UNKNOWN_ERROR",
    UNAUTHORIZED = "UNAUTHORIZED",
    NO_USER_FOUND = "NO_USER_FOUND",
    INVALID_JWT_ERROR = "INVALID_JWT_ERROR",
}

export const ServiceErrors: {
    [key in ErrorCodes]: ErrorDetails;
} = {
    [ErrorCodes.UNKNOWN_ERROR]: {
        type: ErrorCodes.UNKNOWN_ERROR,
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        message: "An unknown error occured"
    },
    [ErrorCodes.UNAUTHORIZED]: {
        type: ErrorCodes.UNAUTHORIZED,
        code: HttpStatus.UNAUTHORIZED,
        message: "Unauthorized"
    },
    [ErrorCodes.NO_USER_FOUND]: {
        type: ErrorCodes.NO_USER_FOUND,
        code: HttpStatus.NOT_FOUND,
        message: "User not found"
    },
    [ErrorCodes.INVALID_JWT_ERROR]: {
        type: ErrorCodes.INVALID_JWT_ERROR,
        code: HttpStatus.UNAUTHORIZED,
        message: "Invalid Jwt"
    }
}
