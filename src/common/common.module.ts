import { Global, Module } from "@nestjs/common";
import { ConfigService } from "./config.service";
import { WinstonModule } from "nest-winston";
import * as winston from "winston";
import { APP_FILTER } from "@nestjs/core";
import { ServiceExceptionFilter } from "./utils/ServiceExceptionFilter";

const loggerOptions: { [key: string]: winston.LoggerOptions } = {
  prod: {
    level: "info",
    transports: [
      new winston.transports.Console()
    ]
  },
  dev: {
    level: "debug",
    transports: [
      new winston.transports.Console(),
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.splat(),
      winston.format.simple()
    ),
    exitOnError: false,
  }
}


@Global()
@Module({
  imports: [
    WinstonModule.forRootAsync({
      useFactory: async (configService: ConfigService) =>
        configService.nodeEnv === "production" ? loggerOptions.prod : loggerOptions.dev,
      inject: [ConfigService],
    })
  ],
  providers: [ConfigService,
    {
      provide: APP_FILTER,
      useClass: ServiceExceptionFilter
    }
  ],
  exports: [ConfigService],
})
export class CommonModule { }
