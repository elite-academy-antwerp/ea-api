import { Injectable } from "@nestjs/common";
import * as dotenv from "dotenv";
import * as yup from "yup";

export interface EnvConfig {
	NODE_ENV: "development" | "production" | "test";
	PORT: number;
	MONGODB_URI: string;
	JWT_SECRET: string;
}

@Injectable()
export class ConfigService {
	private readonly envConfig: EnvConfig;
	private readonly includeVars: (keyof EnvConfig)[] = ["NODE_ENV", "PORT", "MONGODB_URI", "JWT_SECRET"];

	constructor() {
		dotenv.config();

		this.envConfig = this.validateInput(this.includeVars.reduce((config, key) => {
			config[key] = process.env[key] as any;

			return config;
		}, {}) as EnvConfig);
	}

	get port() {
		return this.envConfig.PORT;
	}

	get mongodbUri() {
		return this.envConfig.MONGODB_URI;
	}
	
	get jwtSecret() {
		return this.envConfig.JWT_SECRET;
	}

	get nodeEnv() {
		return this.envConfig.NODE_ENV;
	}

	private validateInput(envConfig: EnvConfig): EnvConfig {
		const schema = yup.object().shape({
			NODE_ENV: yup
				.string()
				.oneOf(["development", "production", "test"])
				.default("development"),
			PORT: yup.number().default(5000),
			MONGODB_URI: yup.string().required(),
			JWT_SECRET: yup.string().required(),
		});

		schema.validateSync(envConfig);

		return schema.cast(envConfig) as EnvConfig;
	}
}
