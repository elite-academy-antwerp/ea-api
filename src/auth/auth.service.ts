import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/user/user.entity';
import { MongoRepository } from 'typeorm';
import * as argon2 from "argon2";

@Injectable()
export class AuthService {
    
    constructor(
        @InjectRepository(User)
        private readonly userRepository: MongoRepository<User>
    ) { }
    
    public async findByEmailAndPassword(email: string, password: string): Promise<User> {
        const user = await this.userRepository.findOne({
            email: email
        });
        if(await argon2.verify(user.password,password)){
            return user
        } else {
            return undefined
        }
    }
    
}
