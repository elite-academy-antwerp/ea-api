import { Resolver, Args, Mutation } from "@nestjs/graphql";
import { AuthService } from "./auth.service";
import { UserService } from "src/user/user.service";
import { JwtService } from "@nestjs/jwt";
import { UnauthorizedException } from '@common/exceptions/unauthorized.exception';
import { TokenResponse } from '@graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Resolver("Auth")
export class AuthResolver {
    constructor(
        private readonly authService: AuthService,
        private readonly jwtService: JwtService
    ) { }
    
    @Mutation(() => TokenResponse)
    public async login(@Args("email") email: string, @Args("password") password: string) {
        const user = await this.authService.findByEmailAndPassword(email, password);
        
        if (!user){
            return null
        }
        
        return {
            id_token: this.jwtService.sign({ sub: user._id, role: user.role, email: user.email, team: user.teamId }, { expiresIn: '1d' })
        }
    }
}
