import { Module } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthResolver } from "./auth.resolver";
import { UserService } from "src/user/user.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "src/user/user.entity";
import { JwtModule, JwtModuleOptions } from "@nestjs/jwt";
import { ConfigService } from '@common/config.service';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { APP_GUARD } from '@nestjs/core';
import { GqlAuthGuard } from './guard/auth.guard';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({ 
      inject: [ConfigService],
			useFactory: async (configService: ConfigService): Promise<JwtModuleOptions> => 
				({
					secret: configService.jwtSecret
				})
    }),
    TypeOrmModule.forFeature([User])
  ],
  providers: [AuthService, AuthResolver, UserService, JwtStrategy]
})
export class AuthModule {}
