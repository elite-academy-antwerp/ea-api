#!/bin/bash

nest g mo ${1}
nest g r ${1}
nest g s ${1}
touch src/${1}/${1}.graphql
touch src/${1}/${1}.entity.ts
touch src/${1}/${1}.input.ts