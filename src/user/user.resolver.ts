
import { Args, Mutation, Query, Resolver, ResolveProperty, Parent } from "@nestjs/graphql";
import { UserInput } from "./user.input";
import { UserService } from "./user.service";
import { User as UserEntity } from "./user.entity";
import { User } from '@graphql';
import { CurrentUser } from '@common/decorators/user.decorator';
import { TeamService } from '@team/team.service';
import { Team } from '@team/team.entity';
import { GqlAuthGuard } from '@auth/guard/auth.guard';
import { UseGuards } from '@nestjs/common';

@Resolver("User")
export class UserResolver {
    constructor(private readonly userService: UserService, private readonly teamService: TeamService) { }

    @Query(() => String)
    public async hello(@CurrentUser() user: User) {
        return "world";
    }

    @Query(() => User)
    @UseGuards(GqlAuthGuard)
    public async me(@CurrentUser() user: User) {

        return this.userService.findUserById(user.id);
    }

    @Query(() => [User])
    @UseGuards(GqlAuthGuard)
    public async users(): Promise<User[]> {
        return this.userService.findAll() as any;
    }

    @Mutation(() => User)
    public async createUser(@Args("input") input: UserInput) {
        return this.userService.create(input);
    }

    @ResolveProperty()
    public async id(@Parent() user: UserEntity) {
        const { _id } = user;
        
        return _id;
    }
    
    @ResolveProperty('team', () => Team)
    public async team(@Parent() user: UserEntity) {
        const { teamId } = user;

        return await this.teamService.findTeamById(teamId);
    }
}
