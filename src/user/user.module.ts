import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Team } from '@team/team.entity';
import { TeamService } from '@team/team.service';
import { User } from "./user.entity";
import { UserResolver } from "./user.resolver";
import { UserService } from "./user.service";

@Module({
  imports: [TypeOrmModule.forFeature([User, Team])],
  providers: [UserResolver, UserService, TeamService]
})
export class UserModule { }
