import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import * as uuid from "uuid";
import { User } from "./user.entity";
import { UserInput } from "./user.input";
import * as argon2 from "argon2";

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: MongoRepository<User>,
    ) { }

    public async findAll(): Promise<User[]> {
        return this.userRepository.find();
    }
    
    public async findUserById(id: string): Promise<User> {
        return this.userRepository.findOne({
            where: {
                _id: id
            }
        });
    }

    public async create(input: UserInput): Promise<User> {
        const user = new User();
        user._id = uuid.v4();
        user.email = input.email;
        user.password = await argon2.hash(input.password);
        user.teamId = input.teamId;
        user.role = input.role;

        return this.userRepository.save(user);
    }
}
