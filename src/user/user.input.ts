export class UserInput {
    email: string;
    password: string;
    teamId: string;
    role: string;
}