import { Entity, Column, ObjectIdColumn } from 'typeorm';

@Entity()
export class Practice {
    @ObjectIdColumn()
    _id: string;
    @Column()
    date: string;
    @Column()
    start: string;
    @Column()
    end: string;
    @Column()
    type: string;
    @Column()
    location: string;
    @Column()
    teamId: string;
}