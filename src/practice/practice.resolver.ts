import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveProperty, Resolver } from '@nestjs/graphql';
import { Practice } from '@graphql';
import { Practice as PracticeEntity } from './practice.entity';
import { PracticeService } from './practice.service';
import { TeamService } from '@team/team.service';
import { PracticeInput } from './practice.input';
import { Team } from '@team/team.entity';
import { GqlAuthGuard } from '@auth/guard/auth.guard';

@Resolver('Practice')
export class PracticeResolver {

    constructor(private readonly practiceService: PracticeService, private readonly teamService: TeamService) { }



    @Query(() => [Practice])
    @UseGuards(GqlAuthGuard)
    public async practices(): Promise<Practice[]> {
        return this.practiceService.findAll() as any;
    }

    @Query(() => Practice)
    @UseGuards(GqlAuthGuard)
    public async practice(@Args('id') id: string): Promise<Practice> {
        return this.practiceService.findPracticeById(id) as any;
    }
    
    @Query(() => [Practice])
    @UseGuards(GqlAuthGuard)
    public async practicesByTeam(@Args('teamId') teamId: string): Promise<Practice[]> {
        return this.practiceService.findPracticeByTeamId(teamId) as any;
    }

    @Mutation(() => Practice)
    @UseGuards(GqlAuthGuard)
    public async createPractice(@Args("input") input: PracticeInput) {
        return this.practiceService.create(input);
    }

    @ResolveProperty()
    public async id(@Parent() practice: PracticeEntity) {
        const { _id } = practice;

        return _id;
    }

    @ResolveProperty('team', () => Team)
    public async team(@Parent() practice: PracticeEntity) {
        const { teamId } = practice;

        return await this.teamService.findTeamById(teamId);
    }
}
