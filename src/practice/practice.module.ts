import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from '@team/team.entity';
import { TeamService } from '@team/team.service';
import { Practice } from './practice.entity';
import { PracticeResolver } from './practice.resolver';
import { PracticeService } from './practice.service';

@Module({
  imports: [TypeOrmModule.forFeature([Practice, Team])],
  providers: [PracticeResolver, PracticeService, TeamService]
})
export class PracticeModule { }
