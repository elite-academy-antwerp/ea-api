import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Practice } from './practice.entity';
import * as uuid from 'uuid'
import { PracticeInput } from './practice.input';

@Injectable()
export class PracticeService {
    constructor(
        @InjectRepository(Practice)
        private readonly practiceRepository: MongoRepository<Practice>,
    ) { }

    public async findAll(): Promise<Practice[]> {
        return this.practiceRepository.find();
    }

    public async findPracticeById(id: string): Promise<Practice> {
        return this.practiceRepository.findOne(id);
    }

    public async findPracticeByTeamId(teamId: string): Promise<Practice[]> {
        return this.practiceRepository.find({ teamId: teamId });
    }

    public async create(input: PracticeInput): Promise<Practice> {
        const practice = new Practice();
        practice._id = uuid.v4();
        practice.start = input.start;
        practice.end = input.end;
        practice.type = input.type;
        practice.location = input.location;
        practice.teamId = input.teamId;
        practice.date = input.date;

        return this.practiceRepository.save(practice);
    }
}
