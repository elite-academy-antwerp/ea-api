import { Test, TestingModule } from '@nestjs/testing';
import { PracticeResolver } from '../practice.resolver';

describe('PracticeResolver', () => {
  let resolver: PracticeResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PracticeResolver],
    }).compile();

    resolver = module.get<PracticeResolver>(PracticeResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
