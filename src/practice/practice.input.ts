export class PracticeInput {
    date: string;
    start: string;
    end: string;
    type: string;
    location: string;
    teamId: string;
}