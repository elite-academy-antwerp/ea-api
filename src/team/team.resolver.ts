import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Parent, Query, ResolveProperty, Resolver } from '@nestjs/graphql';
import { Team } from '@graphql';
import { Team as TeamEntity } from './team.entity';
import { TeamService } from './team.service';
import { TeamInput } from './team.input';
import { UserService } from '@user/user.service';
import { User } from '@user/user.entity';
import { GqlAuthGuard } from '@auth/guard/auth.guard';

@Resolver('Team')
export class TeamResolver {
    constructor(private readonly teamService: TeamService, private readonly userService: UserService) { }



    @Query(() => [Team])
    @UseGuards(GqlAuthGuard)
    public async teams(): Promise<Team[]> {
        return this.teamService.findAll() as any;
    }

    @Query(() => Team)
    @UseGuards(GqlAuthGuard)
    public async team(@Args('id') id: string): Promise<Team[]> {
        return this.teamService.findTeamById(id) as any;
    }

    @Mutation(() => Team)
    @UseGuards(GqlAuthGuard)
    public async createTeam(@Args("input") input: TeamInput) {
        return this.teamService.create(input);
    }

    @ResolveProperty()
    public async id(@Parent() team: TeamEntity) {
        const { _id } = team;

        return _id;
    }

    @ResolveProperty('coach', () => User)
    public async coach(@Parent() team: TeamEntity) {
        const { coachId } = team;

        return await this.userService.findUserById(coachId);
    }

    @ResolveProperty('assistant', () => User)
    public async assistant(@Parent() team: TeamEntity) {
        const { assistentId } = team;

        return await this.userService.findUserById(assistentId);
    }

}
