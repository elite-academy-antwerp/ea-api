export class TeamInput {
    name: string;
    coachId: string;
    assistantId: string;
}