import { Entity, Column, ObjectIdColumn } from 'typeorm';

@Entity()
export class Team {
    @ObjectIdColumn()
    _id: string;
    @Column()
    name: string;
    @Column()
    coachId: string;
    @Column()
    assistentId: string;
}