import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from '@user/user.entity';
import { UserService } from '@user/user.service';
import { Team } from "./team.entity";
import { TeamResolver } from "./team.resolver";
import { TeamService } from "./team.service";

@Module({
    imports: [TypeOrmModule.forFeature([Team, User])],
    providers: [TeamResolver, TeamService, UserService]
})
export class TeamModule { }