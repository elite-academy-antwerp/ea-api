import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Team } from './team.entity';
import { TeamInput } from './team.input';
import * as uuid from "uuid";

@Injectable()
export class TeamService {

    constructor(
        @InjectRepository(Team)
        private readonly teamRepository: MongoRepository<Team>,
    ) { }

    public async findAll(): Promise<Team[]> {
        return this.teamRepository.find();
    }

    public async findTeamById(id: string): Promise<Team> {
        return this.teamRepository.findOne({
            where:
                { _id: id }
        });
    }

    public async create(input: TeamInput): Promise<Team> {
        const team = new Team();
        team._id = uuid.v4();
        team.name = input.name;
        team.coachId = input.coachId;
        team.assistentId = input.assistantId;

        return this.teamRepository.save(team);
    }
}
