# Elite Academy Antwerp NestJS/GraphQL Server
This is a web  app that was started for EA Antwerp but was abandoned because of covid.

The project is origninaly hosted on a private repo from 2dot5makers but for the sake of demonstration it temporarily lives here.

# Run

To get started `.env` should be created according to the `.env.example`
Account can be created by using the graphql playground on the `/graphql` endpoint

To start the server run `npm run start`
